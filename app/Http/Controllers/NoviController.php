<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Novi;
use Exception;
use App\Models\Member as Members;
use App\Models\Stat as Stats;

class NoviController extends Controller
{
    public function index(){

    	$members = Members::all();

    	echo "<ul>";

    	foreach($members as $member){

    		echo '<li><a href="/member/' . $member->UniqueID . '" target="_blank">';
    			echo $member->Name;
    		echo '</a></li>';

    	}

    	echo "</ul>";

    }

    public function member($id){


    	$member = Novi::memberDetail($id);

    	dd($member);


    }

    public function stats(){
    	$Companies = Members::where('CustomerType', "Company")->count();
        $Persons = Members::where('CustomerType', "Person")->count();
        $Divisions = ["Material & Services","Traffic Safety Industry","Public-Private Partnerships","Planning & Design","Contractors","Manufacturers"];

        Stats::create([
            "type" => "MemberCount",
            "value" => $Companies,
            "category" => "Company"
        ]);

        Stats::create([
            "type" => "MemberCount",
            "value" => $Companies,
            "category" => "Person"
        ]);

        foreach ($Divisions as $Division) {
            $count =  Members::where('CustomerType', "Person")->where('PrimaryDivision', $Division)->count();

            Stats::create([
                "type" => $Division,
                "value" => $count,
                "category" => "Person"
            ]);

            $count =  Members::where('CustomerType', "Company")->where('PrimaryDivision', $Division)->count();

            Stats::create([
                "type" => $Division,
                "value" => $count,
                "category" => "Company"
            ]);
        }

        echo "Companies: $Companies | Persons: $Persons";
    }

    public function reset(){
    	$members = Novi::members();


        foreach ($members as $member) {
            try{
                $PrimaryDivision = "";
                $CustomFields = $member["CustomFields"];

                if(array_key_exists("Primary Division for Companies", $CustomFields)){
                    $PrimaryDivision = $CustomFields["Primary Division for Companies"]["Value"];
                }

                Members::updateOrCreate(['UniqueID' => $member["UniqueID"]], [
                    'Active' => $member["Active"],
                    'Approved' => $member["Approved"],
                    'AutoRenew' => $member["AutoRenew"],
                    'County' => $member["County"],
                    'CreatedDate' => $member["CreatedDate"],
                    'CustomerType' => $member["CustomerType"],
                    'CustomFields' => json_encode($CustomFields),
                    'Email' => $member["Email"],
                    'FacebookUrl' => $member["FacebookUrl"],
                    'Fax' => $member["Fax"],
                    'FirstName' => $member["FirstName"],
                    'HideContactInformation' => $member["HideContactInformation"],
                    'HideOnWebsite' => $member["HideOnWebsite"],
                    'Image' => $member["Image"],
                    'InstagramHandle' => $member["InstagramHandle"],
                    'JobTitle' => $member["JobTitle"],
                    'LastName' => $member["LastName"],
                    'LastUpdatedDate' => $member["LastUpdatedDate"],
                    'LinkedInUrl' => $member["LinkedInUrl"],
                    'MemberProfile' => $member["MemberProfile"],
                    'MembershipExpires' => $member["MembershipExpires"],
                    'MemberSince' => $member["MemberSince"],
                    'MemberStatus' => $member["MemberStatus"],
                    'MemberSubStatus' => $member["MemberSubStatus"],
                    'MemberType' => json_encode($member["MemberType"]),
                    'Mobile' => $member["Mobile"],
                    'Name' => $member["Name"],
                    'Notes' => $member["Notes"],
                    'OriginalJoinDate' => $member["OriginalJoinDate"],
                    'ParentCustomerUniqueID' => $member["ParentCustomerUniqueID"],
                    'PersonalEmail' => $member["PersonalEmail"],
                    'PersonalMobile' => $member["PersonalMobile"],
                    'PersonalPhone' => $member["PersonalPhone"],
                    'Phone' => $member["Phone"],
                    'QuickBooksID' => $member["QuickBooksID"],
                    'SpecifiedSystemFields' => json_encode($member["SpecifiedSystemFields"]),
                    'Suffix' => $member["Suffix"],
                    'TenantID' => $member["TenantID"],
                    'PrimaryDivision' => $PrimaryDivision
                ]);
            }
            catch(Exception $e){

            } 
        }
    }

    public function events(){
    	$events = Novi::events();

    	$json = [];

    	foreach($events as $event){
            if($event["EventCategory"]["Name"] == "Safety Training" || $event["EventCategory"]["Name"] == "Safety Webinar"){
                array_push($json, [
                "name" => $event["Name"],
                "active" => $event["Active"],
                "start" => $event["StartDate"],
                "end" => $event["EndDate"],
                "featured" => 0,
                "url" => $event["Url"],
                "timezone" => $event["TimeZone"]
            ]);
            }
    	}

    	echo json_encode($json);
    }
}
