<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Exception;
use Novi;
use App\Models\Member as Members;
use App\Models\Stat as Stats;

class NoviStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Novi:Stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pulls statistics data from Novi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Companies = Members::where('CustomerType', "Company")->count();
        $Persons = Members::where('CustomerType', "Person")->count();
        $Divisions = ["Material & Services","Traffic Safety Industry","Public-Private Partnerships","Planning & Design","Contractors","Manufacturers"];

        Stats::create([
            "type" => "MemberCount",
            "value" => $Companies,
            "category" => "Company"
        ]);

        Stats::create([
            "type" => "MemberCount",
            "value" => $Companies,
            "category" => "Person"
        ]);

        foreach ($Divisions as $Division) {
            $count =  Members::where('CustomerType', "Person")->where('PrimaryDivision', $Division)->count();

            Stats::create([
                "type" => $Division,
                "value" => $count,
                "category" => "Person"
            ]);

            $count =  Members::where('CustomerType', "Company")->where('PrimaryDivision', $Division)->count();

            Stats::create([
                "type" => $Division,
                "value" => $count,
                "category" => "Company"
            ]);
        }

        echo "Companies: $Companies | Persons: $Persons";
    }
}
