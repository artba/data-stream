<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Exception;
use Novi;
use App\Models\Member as Members;
use App\Models\Roster as Rosters;

class NoviRosters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Novi:Rosters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates list of all rosters';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // company rosters

        Rosters::truncate();

        $companies = Members::where('CustomerType', "Company")->get();

        foreach($companies as $company){

            $employees = Members::where('CustomerType', "Person")->where('ParentCustomerUniqueID', $company->UniqueID)->get();

            foreach($employees as $employee){
                Rosters::Create([
                    "group_id" => $company->Name,
                    "member_id" => $employee->Name,
                    "job_title" => $employee->JobTitle,
                    "email" => $employee->Email,
                    "division" => $employee->PrimaryDivision,
                    "type" => 1
                ]);
            }
        }
    }
}
