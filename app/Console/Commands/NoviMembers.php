<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Exception;
use Novi;
use App\Models\Member as Members;
use App\Models\Roster as Rosters;

class NoviMembers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Novi:Members';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pulls all data from Novi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $members = Novi::members();

        foreach ($members as $member) {
            try{
                $PrimaryDivision = "";
                $CustomFields = $member["CustomFields"];

                if(array_key_exists("Primary Division for Company", $CustomFields)){
                    $PrimaryDivision = $CustomFields["Primary Division for Company"]["Value"];
                }

                Members::updateOrCreate(['UniqueID' => $member["UniqueID"]], [
                    'Active' => $member["Active"],
                    'Approved' => $member["Approved"],
                    'AutoRenew' => $member["AutoRenew"],
                    'County' => $member["County"],
                    'CreatedDate' => $member["CreatedDate"],
                    'CustomerType' => $member["CustomerType"],
                    'CustomFields' => json_encode($CustomFields),
                    'Email' => $member["Email"],
                    'FacebookUrl' => $member["FacebookUrl"],
                    'Fax' => $member["Fax"],
                    'FirstName' => $member["FirstName"],
                    'HideContactInformation' => $member["HideContactInformation"],
                    'HideOnWebsite' => $member["HideOnWebsite"],
                    'Image' => $member["Image"],
                    'InstagramHandle' => $member["InstagramHandle"],
                    'JobTitle' => $member["JobTitle"],
                    'LastName' => $member["LastName"],
                    'LastUpdatedDate' => $member["LastUpdatedDate"],
                    'LinkedInUrl' => $member["LinkedInUrl"],
                    'MemberProfile' => $member["MemberProfile"],
                    'MembershipExpires' => $member["MembershipExpires"],
                    'MemberSince' => $member["MemberSince"],
                    'MemberStatus' => $member["MemberStatus"],
                    'MemberSubStatus' => $member["MemberSubStatus"],
                    'MemberType' => json_encode($member["MemberType"]),
                    'Mobile' => $member["Mobile"],
                    'Name' => $member["Name"],
                    'Notes' => $member["Notes"],
                    'OriginalJoinDate' => $member["OriginalJoinDate"],
                    'ParentCustomerUniqueID' => $member["ParentCustomerUniqueID"],
                    'PersonalEmail' => $member["PersonalEmail"],
                    'PersonalMobile' => $member["PersonalMobile"],
                    'PersonalPhone' => $member["PersonalPhone"],
                    'Phone' => $member["Phone"],
                    'QuickBooksID' => $member["QuickBooksID"],
                    'SpecifiedSystemFields' => json_encode($member["SpecifiedSystemFields"]),
                    'Suffix' => $member["Suffix"],
                    'TenantID' => $member["TenantID"],
                    'PrimaryDivision' => $PrimaryDivision
                ]);
            }
            catch(Exception $e){

            } 
        }
    }
}
