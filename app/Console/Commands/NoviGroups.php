<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Exception;
use Novi;
use App\Models\Group as Groups;
use App\Models\Roster as Rosters;

class NoviGroups extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Novi:Groups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pulls all groups data from Novi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Groups::truncate();
        Rosters::truncate();

        $groups = Novi::groups();

        foreach($groups as $group){
            try{
                Groups::Create([
                    "UniqueID" => $group["UniqueID"],
                    "Name" => $group["Name"]
                ]);

                $groupMembers = Novi::groupMembers($group["UniqueID"]);

                foreach ($groupMembers as $groupMember) {

                    Rosters::Create([
                        "group_id" => $group["UniqueID"],
                        "member_id" => $groupMember["UniqueID"]
                    ]);
                }
            }
            catch(Exception $e){
                dd($e);
            }
        }
    }
}
