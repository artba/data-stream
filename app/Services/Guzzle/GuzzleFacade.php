<?php

namespace App\Services\Guzzle;

use Illuminate\Support\Facades\Facade;

class GuzzleFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Guzzle';
    }
}