<?php

namespace App\Services\Novi;

use Illuminate\Support\ServiceProvider;
use App\Services\ValidatorExtended;

class NoviServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Novi', function($app) {
            return new Novi();
        });
    }
}