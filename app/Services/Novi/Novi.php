<?php

namespace App\Services\Novi;
use Guzzle;
use DB;
use Auth;
use Exception;

class Novi
{
	public function members(){
		$members = [];

		for($i = 0; $i <= 15000; $i+=1000){

			try{
				$results = Guzzle::HTTP("GET", env("NOVI_URL") . "/members?pageSize=1000&offset=$i", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

				foreach ($results["response"]["Results"] as $result) {

					array_push($members, $result);
				}
			}
			catch(Exception $e){
				dd($e);
			}
		}

		return $members;
	}

	public function memberDetail($member_id){
		$member = Guzzle::HTTP("GET", env("NOVI_URL") . "/members/$member_id", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		return $member;
	}

	public function groups(){
		$groups = Guzzle::HTTP("GET", env("NOVI_URL") . "/groups", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		return $groups["response"]["Results"];
	}

	public function groupDetail($group_id){
		$group = Guzzle::HTTP("GET", env("NOVI_URL") . "/groups/$group_id", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		return $group["response"]["Results"];
	}

	public function groupMembers($group_id){
		$group = Guzzle::HTTP("GET", env("NOVI_URL") . "/groups/$group_id/members?pageSize=1000", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		return $group["response"];
	}

	public function events(){
		$events = Guzzle::HTTP("GET", env("NOVI_URL") . "/events/event-list", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		return $events["response"];
	}
}