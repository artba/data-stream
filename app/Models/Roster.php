<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roster extends Model
{
    use HasFactory;

    protected $fillable = [
        'created_at', 'updated_at', 'deleted_at', 'member_id', 'group_id', 'type', 'job_title', 'email', 'division'
    ];
}
