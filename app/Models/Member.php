<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    protected $fillable = [
        'created_at', 'updated_at', 'deleted_at', 'Active', 'Approved', 'AutoRenew', 'BillingAddress', 'County', 'CreatedDate', 'CustomerType', 'CustomFields', 'Email', 'FacebookUrl', 'Fax', 'FirstName', 'Groups', 'HideContactInformation', 'HideOnWebsite', 'Image', 'InstagramHandle', 'JobTitle', 'LastName', 'LastUpdatedDate', 'LinkedInUrl', 'MemberProfile', 'MembershipExpires', 'MemberSince', 'MemberStatus', 'MemberSubStatus', 'MemberType', 'Mobile', 'Name', 'Notes', 'OriginalJoinDate', 'ParentCustomerUniqueID', 'PersonalAddress', 'PersonalEmail', 'PersonalMobile', 'PersonalPhone', 'Phone', 'QuickBooksID', 'ShippingAddress', 'SpecifiedSystemFields', 'Suffix', 'TenantID', 'PrimaryDivision', 'UniqueID', 'UnsubscribeFromEmails', 'Website' 
    ];
}
