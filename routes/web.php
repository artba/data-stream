<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NoviController;

Route::get('/safety/events', [NoviController::class, 'events']);
